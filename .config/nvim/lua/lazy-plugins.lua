-- [[ Configure and install plugins ]]
--
--  To check the current status of your plugins, run
--    :Lazy
--
--  You can press `?` in this menu for help. Use `:q` to close the window
--
--  To update plugins you can run
--    :Lazy update
--
-- NOTE: Here is where you install your plugins.
require('lazy').setup({
  -- NOTE: Plugins can be added with a link (or for a github repo: 'owner/repo' link).
  'tpope/vim-sleuth', -- Detect tabstop and shiftwidth automatically

  -- NOTE: Plugins can also be added by using a table,
  -- with the first argument being the link and the following
  -- keys can be used to configure plugin behavior/loading/etc.
  --
  -- Use `opts = {}` to force a plugin to be loaded.
  --

  -- modular approach: using `require 'path/name'` will
  -- include a plugin definition from file lua/path/name.lua

  require 'rc/plugins/gitsigns',

  require 'rc/plugins/which-key',

  require 'rc/plugins/telescope',

  require 'rc/plugins/lspconfig',

  require 'rc/plugins/conform',

  require 'rc/plugins/cmp',

  require 'rc/plugins/todo-comments',

  require 'rc/plugins/mini',

  require 'rc/plugins/treesitter',

  require 'rc/plugins/debug',

  require 'rc/plugins/indent_line',

  require 'rc/plugins/lint',

  require 'rc/plugins/autopairs',

  require 'rc/plugins/neo-tree',

  -- Custom Plugins

  require 'rc/plugins/barbar',

  require 'rc/plugins/transparent',

  require 'rc/plugins/colorizer',

  -- Colorscheme

  -- require 'rc/plugins/colorscheme/tokyonight',
  -- require 'rc/plugins/colorscheme/nightfox',
  require 'rc/plugins/colorscheme/catppuccin',
}, {
  ui = {
    -- If you are using a Nerd Font: set icons to an empty table which will use the
    -- default lazy.nvim defined Nerd Font icons, otherwise define a unicode icons table
    icons = vim.g.have_nerd_font and {} or {
      cmd = '⌘',
      config = '🛠',
      event = '📅',
      ft = '📂',
      init = '⚙',
      keys = '🗝',
      plugin = '🔌',
      runtime = '💻',
      require = '🌙',
      source = '📄',
      start = '🚀',
      task = '📌',
      lazy = '💤 ',
    },
  },
})

-- vim: ts=2 sts=2 sw=2 et
