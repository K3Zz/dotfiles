# Changing "ls" to "exa"
alias ls='exa -l --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -al --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias l.='exa -a | egrep "^\."'

# ..
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

# ls -a after cd
function chpwd() {
    emulate -L zsh
    la
}

# clear & ls -a
alias c="clear && la"

# Colorize grep output
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# Vim
alias v='vim'
alias n='nvim'

# git
alias gau='git add -u'
alias gaa='git add .'
alias gs='git status'
alias gd='git diff'
alias gc='git commit -m'
alias gfom='git fetch origin main'
alias gdom='git diff main origin/main'
alias gl='git pull origin'
alias gp='git push origin'
alias gg='git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset"'
alias ggs='git log --graph --pretty="%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%ar) %C(bold blue)<%an>%Creset" --stat'

# Minecraft
alias mcl='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia __VK_LAYER_NV_optimus=NVIDIA_only minecraft-launcher'
alias atl='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia __VK_LAYER_NV_optimus=NVIDIA_only atlauncher'
alias cfl='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia __VK_LAYER_NV_optimus=NVIDIA_only curseforge'

# system
alias sdn="shutdown now"
